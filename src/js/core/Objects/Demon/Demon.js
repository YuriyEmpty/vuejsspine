import initEvents from './Events/Events'

export default class Demon {

    constructor(app ,id, spineData) {
        this.app = app;
        this.id = id;
        this.spine = new PIXI.spine.Spine(spineData);

        this.init();
    }


    init() {
        this.spine.autoUpdate = true;
        this.spine.scale.set(0.2,0.2)
        this.spine.position.set(300, 300);
        this.spine.state.timeScale = 1;

        initEvents(this);
        this.setAnimation();
        this.app.stage.addChild(this.spine);
    }

    setAnimation(name = "hold", repeat = true) {

        this.spine.state.setAnimation(0, name, repeat);

    }

}