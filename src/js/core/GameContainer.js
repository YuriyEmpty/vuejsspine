import * as PIXI from 'pixi.js'
import Spine from 'pixi-spine'

import Demon from './Objects/Demon/Demon'

export default class GameContainer {
    constructor() {
        this.app = null;

    }

    init() {

        if(PIXI.loader.resources.redDemon === undefined) {
            PIXI.loader.add("redDemon", "./dist/resources/spine/Demon/redDemon.json").load(this.start.bind(this));
        } else {
            PIXI.loader.load(this.start.bind(this));
        }

    }

    start(loader, resources) {
        this.app = new PIXI.Application(1800, 800, {backgroundColor: 0x002B2B2B});
        this.app.stop();


        let demon = new Demon(this.app, 1, resources.redDemon.spineData);

        setTimeout( ()=> {
            demon.attack();
        }, 5000)

        let container = document.getElementById("game-container");

        this.app.start();
        container.appendChild(this.app.view)
    }

}
